package ufro.tdd.utils;

public class Operations {

    public boolean largoTexto(String msje){
        if(msje.length() >= 20){
            return true;
        }else {
            return false;
        }
    }

    public boolean digVerificadorRut(String run){
        boolean validacion = false;
        try {
            run =  run.toUpperCase();
            run = run.replace(".", "");
            run = run.replace("-", "");
            int rutAux = Integer.parseInt(run.substring(0, run.length() - 1));

            char dv = run.charAt(run.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (Exception ignored) {

        }
        return validacion;

    }

}
