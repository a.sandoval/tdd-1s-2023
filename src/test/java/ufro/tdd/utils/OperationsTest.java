package ufro.tdd.utils;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class OperationsTest {

    Operations operations;

    @BeforeEach
    void setUp() {
        operations = new Operations();
    }

    @Test
    @DisplayName("Test mensaje con más de 20 caracteres")
    public void testLargoMsje(){
        String msje = "TDD es un enfoque para programar con una buena practica";

        Assertions.assertTrue(operations.largoTexto(msje));
    }

    @Test
    @DisplayName("Test rut")
    public void testRut(){
        String rut = "20.643.527-5";
        Assertions.assertTrue(operations.digVerificadorRut(rut));
    }

    @AfterEach
    void tearDown() {
    }
}